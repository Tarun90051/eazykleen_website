 

<!DOCTYPE html>
<html lang="en" style="overflow-x: hidden;">

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>EazyKleen Website</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">

 {{-- adding owl corousel --}}

<link rel="stylesheet" href="{{ asset('assets/css/docs.theme.min.css') }}">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owlcarousel/assets/owl.theme.default.min.css') }}">
{{-- animate css --}}
     <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  {{-- animate css ends --}}
  {{-- aos --}}

   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  
  {{-- aos css ends --}}

    





 {{-- ending owl corousel --}}
 <link href="https://fonts.googleapis.com/css2?family=Lato:wght@700&display=swap" rel="stylesheet">
<!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Cookie|Nunito&display=swap" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.5/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{ asset('css2/style.min.css') }}" rel="stylesheet">
  {{-- <link href="{{ asset('css2/styles2.css') }}" rel="stylesheet"> --}}
  <link href="{{ asset('css2/bubble.css') }}" rel="stylesheet">
  <style type="text/css">
    /*html,
    body,
    header,*/
    /*.carousel {
      height: 60vh;
    }*/

    ::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #ed4d13; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #d94814; 
}

    #wave {
  position: relative;
  height: 70px;
  width: 600px;
  background: #e0efe3;
}
#wave:before {
  content: "";
  display: block;
  position: absolute;
  border-radius: 100% 50%;
  width: 340px;
  height: 80px;
  background-color: white;
  right: -5px;
  top: 40px;
}
#wave:after {
  content: "";
  display: block;
  position: absolute;
  border-radius: 100% 50%;
  width: 300px;
  height: 70px;
  background-color: #e0efe3;
  left: 0;
  top: 27px;
}
#bat { 
  display: block;
  
  margin-bottom: 0.5em;
  margin-left: -3px;
 
  border-style: inset;
  border-width: 1.5px;
  margin-top:-25px;
  width: 20%;
} 
#iron { 
  display: block;
  
  margin-bottom: 0.5em;
  margin-left: -1px;
 
  border-style: inset;
  border-width: 1.5px;
  margin-top:-13px;
  width: 22%;
}
#sup { 
  display: block;
  
  margin-bottom: 0.5em;
  margin-left: 475px;
 
  border-style: inset;
  border-width: 1.5px;
  margin-top: -10px;
  width: 10%;
} 
#pro { 
  display: block;
  
  margin-bottom: 0.5em;
  margin-left: 10px;
 
  border-style: inset;
  border-width: 1.5px;
  margin-top: -10px;
  width: 20%;
} 

  

    @media (min-width: 800px) and (max-width: 850px) {
      .navbar:not(.top-nav-collapse) {
        background: #929FBA !important;
      }
    }

  </style>
  <style>

.topnav {
  overflow: hidden;
  background-color:;
}

.topnav a {
  
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 12px 16px;
  text-decoration: none;
  font-size: 17px;
}



.topnav a.active {
  color: blue;
  
}

.topnav .icon {
  display: none;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
  .bgg{
  	height: 140% !important;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }

  .bgg{
  	height: 140% !important;
  }
}

b, strong {
    font-weight: bolder;
}
</style>

</head>

<body style="overflow-x: hidden;">
   <div id="background-wrap">
            <div class="bubble x1"></div>
    <div class="bubble x4"></div>
    <div class="bubble x4"></div>
    <div class="bubble x4"></div>
    
    
  </div>
   <style type="text/css">
        .bgg {
  /* The image used */
  background-image: url("img/asw.png");

  /* Full height */
  height: 115%;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
      </style>
      <style type="text/css">
      	.carousel .carousel-inner, .carousel .carousel-inner .active, .carousel .carousel-inner .carousel-item {

      		height: 120% !important;
      	}
      </style>
      


  <div class="bgg">
  	

  
  
<div class="topnav d-none d-sm-block"  style="overflow-x: hidden;">
  <a><img src="{{ asset('img/logo.png') }}" alt="eazykleen logo" height="40" style="max-width: 60% !important; float: left;margin-left: 130px;"></a>
  
  {{-- <a href="#home" class="active mt-4 mr-5" style="float: right;">Home</a> --}}
  <a href="#contact" class="mt-4" style="float: right;margin-right: 170px;"><b style="color: grey;font-family: 'Roboto',sans-serif;font-weight: bold !important">Contact Us</b></a>
  <a href="#partner" class="mt-4" style="float: right;"><b style="color: grey;font-weight: bold !important;">Partner with us</b></a>
  <a href="#products" class="mt-4 " style="float: right;"><b style="color: grey;font-weight: bold !important">Products</b></a>
  <a href="#about" class="mt-4" style="float: right;"><b style="color: grey;font-weight: bold !important">About Us</b></a>
  <a href="#" class="mt-4 active" style="float: right;"><b style="color: #203c8a;font-weight: bold !important">Home</b></a>

  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>
{{-- mobile view navbar --}}
<div class="topnav d-block d-sm-none" id="myTopnav" style="overflow-x: hidden;">
  <a><img src="{{ asset('img/logo.png') }}" alt="eazykleen logo" height="40" style="max-width: 60% !important; float: left;margin-left: 130px;"></a>
  
  {{-- <a href="#home" class="active mt-4 mr-5" style="float: right;">Home</a> --}}
  <a href="#news" class="mt-4"><b style="color: grey;font-family: 'Roboto',sans-serif;font-weight: bold !important"><br></b></a>
  <a href="#" class="mt-4 active"><b style="color: #203c8a;font-weight: bold !important">Home</b></a>
  <a href="#about" class=""><b style="color: grey;font-weight: bold !important">About Us</b></a>
  <a href="#products" class=""><b style="color: grey;font-weight: bold !important">Products</b></a>
   <a href="#partner" class=""><b style="color: grey;font-weight: bold !important">Partner with us</b></a>
  <a href="#contact" class=""><b style="color: grey;font-weight: bold !important">Contact Us</b></a>

  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars" style="color: black;"></i>
  </a>
</div>
{{-- mobile view ends --}}
<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>


 
  <!--Main layout-->
  <main>
  	
    

      <!--Section: Main info-->
      <section class=" wow fadeIn">


       
            <img src=" {{ asset('img/starr.png') }}" class="img-fluid ml-5" style="max-width: 40%;max-height: 40%;opacity: 0.5;" 
              alt="">
         

        <!--Grid row-->
      
        <div class="row container">

          <!--Grid column-->
          <div class="col-md-6 d-none d-sm-block">

          	<!-- Main heading -->
            <p style="font-size: 55px; color: #19306b;margin-left: 190px;font-weight: 300;" class="mt-5 animate__animated animate__fadeInDown">The Ultimate</p>
            <h3 style="color: #19306b;margin-top: -20px;margin-left: 190px;font-size: 55px;" class="animate__animated animate__fadeInDown">Germ Killer</h3>
            
            <!-- Main heading -->

            

          </div>
          {{-- mobile view of heading --}}
           <div class="col-sm-6 d-block d-sm-none">

            <!-- Main heading -->
            <p style="font-size: 45px; color: #19306b;font-weight: 300;text-align: center;" class="mt-4 animate__animated animate__fadeInDown ">The Ultimate</p>
            <h3 class="h1" style="color: #19306b;margin-top: -20px;text-align: center;"class="animate__animated animate__fadeInDown">Germ Killer</h3>
            
            <!-- Main heading -->

            

          </div>
          {{-- mobile view ends --}}
          
          <!--Grid column-->

          <!--Grid column-->
          
          <div class="col-sm-6 d-none d-sm-block">



          	

            {{-- <img src="{{ asset('img/liquid.png') }}" class="img-fluid animate__animated animate__bounceInRight" id="img" 
              alt="" style="margin-left: 170px;float: left;" > --}}
              <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-pause="false">
  <div class="carousel-inner" style="margin-left: 160px;">
    <div class="carousel-item active">
      <img src="{{ asset('img/liquid.png') }}" alt="First slide">
    </div>
    <div class="carousel-item">
      <img  src="{{ asset('img/slider2.png') }}" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img/slider3.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img src="{{ asset('img/slider4.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img src="{{ asset('img/slider5.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img src="{{ asset('img/slider6.png') }}" alt="Third slide">
    </div>
  </div>
 </div>
          </div>

            


         
          <!--Grid column-->

          {{-- mobile view of photo --}}
          <div class="col-sm-12 d-block d-sm-none mt-4">

           <div class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block " src="{{ asset('img/slider1.png') }}" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block " src="{{ asset('img/slider2.png') }}" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block " src="{{ asset('img/slider3.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img class="d-block " src="{{ asset('img/slider4.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img class="d-block " src="{{ asset('img/slider5.png') }}" alt="Third slide">
    </div>
     <div class="carousel-item">
      <img class="d-block" src="{{ asset('img/slider6.png') }}" alt="Third slide">
    </div>
  </div>
 </div>
          </div>

            


          </div>
          <!--Grid column-->
          {{-- mobile view of photo ends --}}

        
        <!--Grid row-->
      {{--   <svg viewBox="0 0 1300 219">
        <path fill="#fff" fill-opacity="1" d="M0,32L48,80C96,128,192,224,288,224C384,224,480,128,576,90.7C672,53,768,75,864,96C960,117,1056,139,1152,149.3C1248,160,1344,160,1392,160L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
      </svg> --}}
    
</div>
</div>


     {{-- <img src=" {{ asset('img/leaf.png') }}" class="img-fluid"
              alt="" style="float: right;"> --}}
    

      </section>
  
      <!--Section: Main info-->
      <!--Section: Main features & Quick Start-->
      <section class="container">
      	 <img src=" {{ asset('img/flower.png') }}" class="img-fluid"
              alt="">
         


        <!--Grid row-->
        
        <div class="row wow fadeIn" id="about">

          <!--Grid column-->

          <div class="col-lg-6 col-md-12 px-4">
          	

            <img src=" {{ asset('img/cluster.png') }}" class="img-fluid mt-5"
              alt="">
          


          </div>
          <!--/Grid column-->

          <!--Grid column-->

          
          <div class="col-lg-6 col-md-12">

            <p class="h1 mb-4 mt-4" style="color: grey;position: relative;">About Us</p>
            <hr id="bat" style="position: absolute;">

            <div>
             <p>We aim to provide high quality sanitation and disinfectant product to Indian market at an economical price, especially to rural parts of india where sanitation is not yet Primary focus because of the unavailability of inexpensive product.</p>
            </div>
           <button type="button" class="btn btn-deep-orange">Read More</button>
          </div>
          <!--/Grid column-->

        </div>
        <!--/Grid row-->

      </section>
      <!--Section: Main features & Quick Start-->

     

     

      <!--Section: Not enough-->
      <section class="container" id="products">


        <div>
      
        <h2 class="h1 text-center d-none d-sm-block" style="margin-top: 60px;color: grey;">Products</h2>
        <h2 class="h1 text-center d-block d-sm-none" style="margin-top: 60px;color: grey; margin-right: 200px;">Products</h2>
      </div>
      <hr id="pro" class="d-block d-sm-none">
        <hr id="sup" class="d-none d-sm-block">
       <!--Carousel Wrapper-->
       
       <div class="carousel-wrap d-none d-sm-block">
       <div class="row mt-4 ml-5">
        <div class="large-12 columns mt-5">
         <div class="owl-carousel">
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/cleaner.png') }}" class="img-fluid"
              alt="" style="max-width: 70%;">
              <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 70%;position: absolute;top: 120px;left: 20px;z-index: -1;">
              <p class="mt-4 ml-5">Perfumed Floor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
             <img src=" {{ asset('img/household.png') }}" class="img-fluid"
              alt="" style="max-width: 70%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 70%;position: absolute;top: 120px;left: 20px;z-index: -1;">
              <p class="mt-4 ml-5">Glass & Household<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/toilet.png') }}" class="img-fluid"
              alt="" style="max-width: 65%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 70%;position: absolute;top: 120px;left: 10px;z-index: -1;">
               <p class="mt-4 ml-5">Toilet Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/clnn.png') }}" class="img-fluid"
              alt="" style="max-width: 86%;height: 70%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 80%;position: absolute;top: 110px;left: 10px;z-index: -1;">
               <p class="mt-2 ml-5">Handwash Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/prod6.png') }}" class="img-fluid mt-4"
              alt="" style="max-width: 66%;height: 70%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 68%;position: absolute;top: 110px;left: 10px;z-index: -1;">
               <p class="mt-4 ml-5">Bathroom Cleaner</p>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  
    {{-- mobile view of corousel --}}
     <div class="carousel-wrap d-block d-sm-none">
       <div class="row mt-3 ml-2">
        <div class="large-12 columns mt-5">
         <div class="owl-carousel">
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/cleaner.png') }}" class="img-fluid"
              alt="" style="max-width: 90%;">
              <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 90%;position: absolute;top: 120px;left: 20px;z-index: -1;">
              <p class="mt-4" style="margin-left: 88px;">Perfumed Floor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
             <img src=" {{ asset('img/household.png') }}" class="img-fluid"
              alt="" style="max-width: 90%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 90%;position: absolute;top: 120px;left: 30px;z-index: -1;">
              <p class="mt-4" style="margin-left: 88px;">Glass & Household<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/toilet.png') }}" class="img-fluid"
              alt="" style="max-width: 85%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 95%;position: absolute;top: 120px;left: 6px;z-index: -1;">
               <p class="mt-4" style="margin-left: 88px;">Toilet Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/clnn.png') }}" class="img-fluid"
              alt="" style="max-width: 95%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 85%;position: absolute;top: 120px;left: 6px;z-index: -1;">
               <p class="mt-4" style="margin-left: 83px;">Handwash Cleaner</p>
            </div>
            <div class="item" style="position: relative;">
              <img src=" {{ asset('img/prod6.png') }}" class="img-fluid"
              alt="" style="max-width: 85%;">
               <img src=" {{ asset('img/cover.png') }}" class="img-fluid"
              alt="" style="max-width: 95%;position: absolute;top: 120px;left: 6px;z-index: -1;">
               <p class="mt-5" style="margin-left: 83px;">Bathroom Cleaner</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- mobile view of corousel ends --}}



<!--/.Carousel Wrapper-->
</section>


        
      <!--Section: form-->
      <section style="margin-top: 90px;" class="container" id="partner">


        <!--Grid row-->
        <div class="row wow fadeIn mt-5">

          <!--Grid column-->
          <div class="col-lg-6 col-md-12 d-none d-sm-block px-4">

            <h1 style="font-size: 40px;color: grey;" >Partner with us</h1>
            <hr id="iron">
           {{--  <h1 style="font-size: 50px; margin-top: 15px;color: #e84117;"><b>Why&nbsp;&nbsp;?</b></h1> --}}
            <p class="mt-4"></p>



          </div>

          <!--/Grid column-->
          {{-- mobile view of partner with us --}}
         

          <!--Grid column-->
          <div class="col-lg-6 col-md-12 d-block d-sm-none" style="margin-top: -70px;">

            <h1 style="font-size: 40px;color: grey;">Partner with us</h1>
            <hr id="iron">
           {{--  <h1 style="font-size: 50px; margin-top: 15px;color: #e84117;"><b>Why&nbsp;&nbsp;?</b></h1> --}}
            <p class="mt-4"></p>



          </div>
          {{-- mobile view of partner with us --}}

          <!--Grid column-->
          <div class="col-lg-6 col-md-12">
            @if($message= Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">x</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
              <form method="POST" action="{{ url('/sendemail') }}">
    {{ csrf_field() }}

            <div class="md-form input-group mb-3">
  <div class="input-group-prepend">
    
  </div>

  <input type="text" class="form-control" name="name" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default" placeholder="Name :" required>

</div>
<div class="md-form input-group mb-3">
  <div class="input-group-prepend">
    
  </div>
  <input type="text" class="form-control" aria-label="Sizing example input" name="email" aria-describedby="inputGroupMaterial-sizing-default" placeholder="Email :" required>
  
</div>
<div class="md-form input-group mb-3">
  <div class="input-group-prepend">
    
  </div>
  <input type="text" class="form-control" name="number" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default" placeholder="Number :" required>
  
</div>
<div class="md-form input-group mb-3">
  <div class="input-group-prepend">
    
  </div>
  <input type="text" class="form-control" name="place" aria-label="Sizing example input" aria-describedby="inputGroupMaterial-sizing-default" placeholder="Place :" required>
  
</div>
<div class="md-form input-group mb-3">
  <div class="input-group-prepend">
    
  </div>
  <input type="text" class="form-control" aria-label="Sizing example input" name="description" aria-describedby="inputGroupMaterial-sizing-default" placeholder="Discription :" required>
  
</div>
           <button type="submit" class="btn btn-deep-orange">Submit</button>
       </form>
          </div>
          <!--/Grid column-->

        </div>
        <!--/Grid row-->

      </section>
      <!--Section: Main features & Quick Start-->

      <style type="text/css">
        .bg {
  /* The image used */
  background-image: url("img/last.png");

  /* Full height */
  

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
      </style>

      <div class="bg">

      
 
      <section class="wow fadeIn">

        <!--Grid row-->
        <div class="row">

          <!--Grid column-->
          <div class="col-md-6 d-none d-sm-block mt-5 container">

          	<!-- Main heading -->
            <img src="{{ asset('img/handwash.png') }}" class="img-fluid ml-4"
              alt="" style="max-width: 60%;float: right;">

            
            <!-- Main heading -->

            

          </div>
          {{-- mobile view of handwash --}}
          <div class="col-md-6 d-block d-sm-none mt-5 container">

            <!-- Main heading -->
            <img src="{{ asset('img/handwash.png') }}" class="img-fluid ml-5"
              alt="" style="max-width: 60%;float: left;">

            
            <!-- Main heading -->

            

          </div>
          {{-- mobile view of handwash ends --}}
          
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-md-6 container d-none d-sm-block" style="margin-top: 120px;">

            <img src="{{ asset('img/flower.png') }}" class="img-fluid"
              alt="" style="max-width: 60%;float: right;margin-left: 100px;">


            

            


          </div>

           <!--mobile view of flower-->
          <div class="col-md-6 container d-block d-sm-none" style="margin-top: 50px;">

            <img src="{{ asset('img/flower.png') }}" class="img-fluid"
              alt="" style="max-width: 60%;float: right;margin-left: 100px;">


            

            


          </div>
          <!--mobile view of flower ends-->

        </div>
        <!--Grid row-->

      </section>


     

      <!--Section: Not enough-->
      
     
 
  
  <!--Main layout-->

  <!--Footer-->
  <style>
.vl {
  border-left: 1.5px solid grey;
  height: 130px;
}
</style>
  <section class=" text-center font-small wow fadeIn" id="contact">

    <!--Call to action-->
    <div class="pt-4 container">
     <div class="row">
     	<div class="col-md-2 ">
     		<img src="{{ asset('img/logo.png') }}">
     	</div>
     	
     		<div class="vl d-none d-sm-block"></div>
     	
     	<div class="col-md-5 d-none d-sm-block ml-5 mt-3" style="text-align: left;">
     		<p style="color: grey;">Contact Us:</p>
     		<p style="color: grey;">AIC customer care: 8073891299</p>
     		<p style="color: grey;">contact@aicchemical.in | www.eazykleen.in </p>
     	</div>
      {{-- mobile view of contact us --}}
      <div class="col-md-5 d-block d-sm-none ml-2 mt-3" style="text-align: left;">
        <p style="color: grey;">Contact Us:</p>
        <p style="color: grey;">AIC customer care: 8073891299</p>
        <p style="color: grey;">contact@aicchemical.in | www.eazykleen.in </p>
      </div>
      {{-- mobile view of contact us ends --}}
     	<div class="col-md-4 ml-2 mt-3" style="text-align: left;">
     		<p style="color: grey;">MANUFACTURED AND MARKETED BY :</p>
     		<p style="color: grey;">AIC Chemicals, Vasanti Krupa, 1st floor, 1st cross, Bhagyanagar Belagavi-6, Karnataka.</p>
     		
     	</div>
    </div>
</div>
    <!--/.Call to action-->

    <hr class="my-4">

    <!-- Social icons -->
    <div class="row">
    	<div class="col-md-6">
    		<p style="margin-left: 40px; color: grey;"> © Copyright:2020. All rights reserved by Eazy Kleen </p>
    	</div>
    	
    	<div class="col-md-4 col offset-md-2">
    		 <a href="" style="color: grey;">
        <i class="fab fa-facebook-f mr-3"></i>
      </a>

      <a href="" style="color: grey;">
        <i class="fab fa-twitter mr-3"></i>
      </a>
       <a href="" style="color: grey;">
        <i class="fab fa-linkedin mr-3"></i>
      </a>

    	</div>
     
     

    
    </div>
    <!-- Social icons -->



  </section>
</main>

</div>
  
  <!--/.Footer-->

  <!-- SCRIPTS -->
<!-- SCRIPTS -->

    <!-- JQuery -->
    {{-- for owl corousel --}}
    
    <script src="{{ asset('assets/vendors/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/owlcarousel/owl.carousel.js') }}"></script>

    {{-- for owl corousel --}}
    <!-- vendors -->
    <script src="{{ asset('assets/vendors/highlight.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    {{-- vendors end --}}
    {{--  --}}
    <script>
      var owl = $('.owl-carousel');
      owl.owlCarousel({
      	 autoplay: true,
                autoplayTimeout: 3000,
                navigation: true,
        margin: 10,
        loop: true,
         navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
          1000: {
            items: 3
          }
        }

      })
     
    </script>
    
     <!-- javascript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.5/js/mdb.min.js"></script>
    <!-- Initializations -->
    <script type="text/javascript">
    // Animations initialization
    new WOW().init();
     

</script>
 <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
   <script>
  AOS.init();
</script>
<script type="text/javascript">
	$('.carousel').carousel({
  interval: 2000
})
</script>

</body>

</html>
