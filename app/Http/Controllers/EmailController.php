<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class EmailController extends Controller
{
    //
    function index(Request $request)
    {

    	$this->validate($request,[

    		'name' => 'required',
    		'email'=> 'required|email',
    		'number'=>'required',
    		'place'=>'required',
    		'description'=>'required'


    	]);
    	$data= array(

    		'name' =>$request->name,
    		'email'=>$request->email,
    		'number'=>$request->number,
    		'place'=>$request->place,
    		'description'=>$request->description



    	);

    	Mail::to('rajatsrivastava1600@gmail.com')->send(new SendMail($data));

    	return back()->with('success','Thanks for contacting us!');
    }
}
